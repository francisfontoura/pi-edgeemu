#!/usr/bin/env python
import ez_setup
ez_setup.use_setuptools()
from setuptools import setup, find_packages

setup(
    name = 'edge|emulation for dialog',
    version = '0.2',
    packages = find_packages(),
    scripts = ['bin/edgeemu'],

    install_requires = ['requests', 'beautifulsoup4', 'python2-pythondialog'],

    author = 'Francis Vagner dos Anjos Fontoura',
    author_email = 'francisfontoura@gmail.com',
    description = 'A dialog (formerly, cdialog) frontend to edge|emulation (edgeemu.net), primarily focused on Raspberry Pi (RetroPie project).',
    license = 'MIT',
    url = 'https://bitbucket.org/francisfontoura/pi-edgeemu'
)
